# README #

This repo containts the following:

- a tcl script for improving Vivado performance. Save it as init.tcl under your Vivado scripts directory, e.g., C:\xilinx\Vivado\2016.4\scripts. For more info, see http://dr-j-digital-fun.blogspot.com/2018/03/ece-440-turbo-charge-vivado.html

- a folder with the zybo board files. Place the entire B.3 folder under the Zybo board files directory, e.g., C:\xilinx\Vivado\2016.4\data\boards\board_files\zybo.

- a generic XDC file to use with the original Zybo board: https://store.digilentinc.com/zybo-zynq-7000-arm-fpga-soc-trainer-board/

### Version ###

The board files came from https://github.com/Digilent/vivado-boards/tree/master/new/board_files/zybo/B.3

The XDC file came from https://github.com/Digilent/digilent-xdc/blob/master/Zybo-Master.xdc



### Who do I talk to? ###

Dr. J