To improve performance
======================

Adjust the max threads parameter in init.tcl to match your CPU thread limit
and place the file under the following directory:

<Xilinx Install Dir>\Vivado\<version>\scripts

For example:

C:\Xilinx\Vivado\2016.4\scripts\init.tcl

Then, each time Vivado runs it should use the max threads. (You may still need to set the number
of jobs to the number of cores)

Every time you log in, set the Power Settings under Control Panel\Hardware to High Performance

To install the Zybo board files
===============================

Place the entire zybo folder under the following directory:

<Xilinx Install Dir>\Vivado\<version>\data\boards\board_files

For example:

C:\Xilinx\Vivado\2016.4\data\boards\board_files\zybo

Generic XDC file for Zybo Board
===============================

Copy Zybo-Master.xdc to your project directory and then uncomment the lines you need
by removing the leading '#' character.

For example,

##Clock signal
set_property -dict { PACKAGE_PIN L16   IOSTANDARD LVCMOS33 } [get_ports { clk }]; #IO_L11P_T1_SRCC_35 Sch=sysclk
create_clock -add -name sys_clk_pin -period 8.00 -waveform {0 4} [get_ports { clk }];





